const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class DatosAPI extends RESTDataSource {

    constructor() {
        super();
        this.baseURL = serverConfig.datos_api_url; 
    }

    async createAccount(datos) {
        datos = new Object(JSON.parse(JSON.stringify(datos)));
        return await this.post('/accounts', datos); 
    }

    async accountByUsername(username) {
        return await this.get(`/accounts/${username}`);
    }

    async createTransaction(transaction) {
        transaction = new Object(JSON.parse(JSON.stringify(transaction))); 
        return await this.post('/transactions', transaction);
    }

    async transactionByUsername(username) {
        return await this.get(`/transactions/${username}`);
    } 
}

module.exports = DatosAPI;