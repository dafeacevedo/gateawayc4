const datosResolver = { 
    Query: {
        datosByUsername: async(_, { username }, { dataSources, userIdToken }) => { 
            usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username 
            if (username == usernameToken)
                return await dataSources.datosAPI.datosByUsername(username) 
            else
                return null
        }, 
    },
    Mutation: {}
};

module.exports = datosResolver;