const usersResolver = { 
    Query: {
        userDetailById: (_, { userId }, { dataSources, userIdToken }) => { 
            if (userId == userIdToken)
                return dataSources.authAPI.getUser(userId) 
            else
                return null
        }, 
    },
    Mutation: {
        signUpUser: async(_, { userInput }, { dataSources }) => {
            const datosInput = {
                username: userInput.username, 
                idiomas: userInput.idiomas,
                lvl_idiomas: userInput.lvl_idiomas,
                lvl_academico: userInput.lvl_academico,
                titulo: userInput.titulo,
                institucion: userInput.institucion,
                años_exp: userInput.años_exp
            }
            await dataSources.datosAPI.createAccount(datosInput);

            const authInput = {
                username: userInput.username, 
                password: userInput.password, 
                name: userInput.name,
                email: userInput.email,
                identificacion: userInput.identificacion,
                telefono: userInput.telefono,
                Profesion: userInput.Profesion,
            }
            return await dataSources.authAPI.createUser(authInput); 
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.authAPI.authRequest(credentials),

        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.authAPI.refreshToken(refresh),
    }
};

module.exports = usersResolver;