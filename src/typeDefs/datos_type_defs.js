const { gql } = require('apollo-server');

const datosTypeDefs = gql ` 
    type datos {
        username: String!
        idiomas: String!
        lvl_idiomas: String!
        lvl_academico: String!
        titulo: String!
        institucion: String!
        años_exp: String!
    }

    extend type Query {
        datosByUsername(username: String!): datos
    } 
`;

module.exports = datosTypeDefs;

